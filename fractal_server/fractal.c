#include <complex.h>
#include <pthread.h>
#include <limits.h>
#include <stdlib.h>
#include <stdio.h>
#include <math.h>

double complex f(double complex x, double complex c)
{
    return cpowf(x, 5.0) + c;
}

double complex df(double complex x)
{
    return 5.0 * cpowf(x, 4.0);
}

double complex newtons_method(double complex x, double complex c, int iterations)
{
    for(int i = 0; i < iterations; i++)
    {
        x = x - ((x / 5) + c/df(x));
    }

    return x;
}

double complex* get_true_roots(double c)
{
    double complex* array = malloc(sizeof(double complex[5]));

    array[0] = -(cpow(-1.0, 0.0 / 5.0) * cpow(c, 1.0 / 5.0));
    array[1] =  (cpow(-1.0, 1.0 / 5.0) * cpow(c, 1.0 / 5.0));
    array[2] = -(cpow(-1.0, 2.0 / 5.0) * cpow(c, 1.0 / 5.0));
    array[3] =  (cpow(-1.0, 3.0 / 5.0) * cpow(c, 1.0 / 5.0));
    array[4] = -(cpow(-1.0, 4.0 / 5.0) * cpow(c, 1.0 / 5.0));

    return array;
}

char get_closest_root(double complex* roots, double complex point, char root_count)
{
    int min = INT_MAX;
    char min_i = -1;

    for(char i = 0; i < root_count; i++)
    {
        double distance = (creal(roots[i]) - creal(point)) * (creal(roots[i]) - creal(point)) +\
                          (cimag(roots[i]) - cimag(point)) * (cimag(roots[i]) - cimag(point));

        if(distance < min)
        {
            min_i = i;
            min = distance;
        }
    }

    return min_i;
}

#define NUM_THREADS 4  // You can adjust the number of threads as needed

typedef struct {
    int start_x;
    int end_x;
    int pixels;
    double origin_x;
    double origin_y;
    double scale;
    double c;
    int iterations;
    double complex* roots;
    char* matrix;
} ThreadData;

void* calculate_fractal_matrix_thread(void* arg) {
    ThreadData* data = (ThreadData*)arg;
    int pixels = data->pixels;
    double step = 2 * data->scale / pixels;

    for (int x = data->start_x; x < data->end_x; x++) {
        for (int y = 0; y < pixels; y++) {
            double complex point = CMPLX(
                data->origin_x - data->scale + (step * x),
                data->origin_y - data->scale + (step * y)
            );

            point = newtons_method(point, data->c, data->iterations);

            data->matrix[x + y * pixels] = get_closest_root(data->roots, point, 5) + '0' + 1;
        }
    }

    pthread_exit(NULL);
}

char* calculate_fractal_matrix(
    int pixels, double origin_x, double origin_y, double scale, double c, int iterations
) {
    double complex* roots = get_true_roots(c);

    char* matrix = malloc(sizeof(char) * pixels * pixels);

    pthread_t threads[NUM_THREADS];
    ThreadData thread_data[NUM_THREADS];

    int chunk_size = pixels / NUM_THREADS;
    for (int i = 0; i < NUM_THREADS; i++) {
        thread_data[i].start_x = i * chunk_size;
        thread_data[i].end_x = (i == NUM_THREADS - 1) ? pixels : (i + 1) * chunk_size;
        thread_data[i].pixels = pixels;
        thread_data[i].origin_x = origin_x;
        thread_data[i].origin_y = origin_y;
        thread_data[i].scale = scale;
        thread_data[i].c = c;
        thread_data[i].iterations = iterations;
        thread_data[i].roots = roots;
        thread_data[i].matrix = matrix;

        pthread_create(&threads[i], NULL, calculate_fractal_matrix_thread, &thread_data[i]);
    }

    for (int i = 0; i < NUM_THREADS; i++) {
        pthread_join(threads[i], NULL);
    }

    free(roots);

    return matrix;
}
