#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <stdio.h>
#include <ws.h>

extern char* calculate_fractal_matrix(
    int pixels, double origin_x, double origin_y, double scale, double c, int iterations
);

void onopen(ws_cli_conn_t *client)
{
    char *cli;
    cli = ws_getaddress(client);
    printf("Connection opened, addr: %s\n", cli);
}

void onclose(ws_cli_conn_t *client)
{
    char *cli;
    cli = ws_getaddress(client);
    printf("Connection closed, addr: %s\n", cli);
}

void onmessage(ws_cli_conn_t *client, const unsigned char *msg, uint64_t size, int type)
{
    char *cli;
    cli = ws_getaddress(client);
    printf("Received a message: %s (%zu), from: %s\n", msg, size, cli);

    int pixels = atoi(strtok(msg, "|"));

    double origin_x = atof(strtok(NULL, "|"));
    double origin_y = atof(strtok(NULL, "|"));
    double scale = atof(strtok(NULL, "|"));
    double c = atof(strtok(NULL, "|"));

    int iterations = atoi(strtok(NULL, "|"));

    char* buffer = calculate_fractal_matrix(
        pixels, origin_x, origin_y, scale, c, iterations
    );

    ws_sendframe_txt(client, buffer);
}

int main(void)
{
    struct ws_events evs;
    evs.onopen    = &onopen;
    evs.onclose   = &onclose;
    evs.onmessage = &onmessage;

    ws_socket(&evs, 8483, 0, 1000);

    return 0;
}
