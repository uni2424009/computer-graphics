const socket = new WebSocket("ws://localhost:8483");

const rangeInput = document.getElementById('zoom-range');
const label = document.getElementById('zoom-label');

var buffer_cache = null;

socket.addEventListener("open", () => {
    socket.addEventListener("message", (event) => {
        var buffer = event.data;
        buffer_cache = buffer;

        draw_fractal(buffer, Math.round(Math.sqrt(buffer.length)));
    });
});

socket.addEventListener("error", () => {
    var myModal = new bootstrap.Modal(document.getElementById('serverConnectionErrorModal'));
    myModal.show();
});

const inputC = document.getElementById('inputC');
let previousValue = null;
let previousRangeValue = null;

setInterval(() => {
    var val = inputC.value;

    if(!val)
    {
        val = 1;
    }

    if (inputC.value !== previousValue || rangeInput.value != previousRangeValue) {
        socket.send("400|0|1|" + sliderToLogarithmic(1000 - rangeInput.value).toFixed(5)
         + "|" + val + "|100"); 

        previousValue = val;
        previousRangeValue = rangeInput.value;
    }
}, 500);

const canvas = document.getElementById('canvas');
const ctx = canvas.getContext('2d');

const virtualPixelSize = 400;

const fractal_colors = ['#000000', '#4c2779', '#6a36a9', '#8a56c9', '#ac86d8', '#cdb6e8']

function draw_fractal(buffer, size) {
    const scaleX = size / virtualPixelSize;
    const scaleY = size / virtualPixelSize;

    const scale = Math.min(scaleX, scaleY);

    const canvasWidth = virtualPixelSize * scale;
    const canvasHeight = virtualPixelSize * scale;

    canvas.width = canvasWidth;
    canvas.height = canvasHeight;

    ctx.setTransform(scale, 0, 0, scale, 0, 0);

    for(x = 0; x < size; x++)
    {
        for(y = 0; y < size; y++)
        {
            color = fractal_colors[buffer[x + y*size]];

            ctx.fillStyle = color;
            ctx.fillRect(x, y, 1, 1);
        }
    }
}

rangeInput.addEventListener('input', updateLabelText);

function updateLabelText() {
    label.textContent = `Приближення: ${sliderToLogarithmic(1000 - rangeInput.value).toFixed(5)}`;
}

function sliderToLogarithmic(sliderValue) {
    const logarithmicMin = Math.log10(0.00005);
    const logarithmicMax = Math.log10(5);
    const minSlider = 0;
    const maxSlider = 1000;
    
    const logarithmicValue = Math.pow(10, (logarithmicMin + (logarithmicMax - logarithmicMin) * (sliderValue - minSlider) / (maxSlider - minSlider)));
    return logarithmicValue;
}

function hueToRgb(h) {
    h = (h % 360 + 360) % 360;
    s = 1;
    v = 1;

    const c = v * s;
    const x = c * (1 - Math.abs((h / 60) % 2 - 1));
    const m = v - c;

    let r, g, b;

    if (h < 60) {
        r = c;
        g = x;
        b = 0;
    } else if (h < 120) {
        r = x;
        g = c;
        b = 0;
    } else if (h < 180) {
        r = 0;
        g = c;
        b = x;
    } else if (h < 240) {
        r = 0;
        g = x;
        b = c;
    } else if (h < 300) {
        r = x;
        g = 0;
        b = c;
    } else {
        r = c;
        g = 0;
        b = x;
    }

    return {
        r: Math.round((r + m) * 255),
        g: Math.round((g + m) * 255),
        b: Math.round((b + m) * 255),
    };
}

function componentToHex(c) {
    var hex = c.toString(16);
    return hex.length == 1 ? "0" + hex : hex;
}

function rgbToHex(r, g, b) {
    return "#" + componentToHex(r) + componentToHex(g) + componentToHex(b);
}


const hueInput = document.getElementById('hsl-hue');

hueInput.addEventListener('input', () => {
    var initialColor = hueToRgb(hueInput.value);

    for(var i = 3; i < 8; i++)
    {
        var r = initialColor.r * i/9;
        var g = initialColor.g * i/9;
        var b = initialColor.b * i/9;

        fractal_colors[i-2] = rgbToHex(Math.round(r), Math.round(g), Math.round(b));
    }

    draw_fractal(buffer_cache, Math.round(Math.sqrt(buffer_cache.length)));
});





// Questions and Answers
const questions = [
  {
    question: 'Що таке фрактал Ньютона?',
    options: ['Математична теорема', 'Тип геометричної фігури', 'Фрактал', 'Поняття у квантовій фізиці'],
    answer: 'Фрактал'
  },
  {
    question: 'Яку математичну концепцію досліджує фрактал Ньютона?',
    options: ['Теорію хаосу', 'Ітераційні методи', 'Диференціальні рівняння', 'Теорія чисел'],
    answer: 'Ітераційні методи'
  },
  {
    question: 'Що з наведеного нижче найкраще описує зовнішній вигляд ньютонівського фракталу?',
    options: ['Гладкі, регулярні візерунки', 'Випадкові, неправильні форми', 'Самоподібні, складні структури', 'Ідеальні кола та квадрати'],
    answer: 'Самоподібні, складні структури'
  },
  {
    question: 'Що в методі Ньютона представляє фрактал?',
    options: ['Корені комплексної функції', 'Прості числа в послідовності', 'Розв\'язки лінійних рівнянь', 'Факторіальні обчислення'],
    answer: 'Корені комплексної функції'
  },
  {
    question: 'Який тип рівнянь зазвичай використовується для генерації фрактала Ньютона?',
    options: ['Тригонометричні рівняння', 'Поліноміальні рівняння', 'Експоненціальні рівняння', 'Дифереціальні рівняння'],
    answer: 'Поліноміальні рівняння'
  },
  {
    question: 'Яку роль відіграє ітерація у формуванні фрактала Ньютона?',
    options: ['Вона визначає кольори фрактала', 'Створює візерунки, що повторюються', 'Уточнює наближення для знаходження коренів', 'Формує межі фрактала'],
    answer: 'Уточнює наближення для знаходження коренів'
  },
  {
    question: 'Який вид симетрії спостерігається у фракталі Ньютона?',
    options: ['Обертальна симетрія', 'Трансляційна симетрія', 'Відбивна симетрія', 'Фрактальна симетрія'],
    answer: 'Фрактальна симетрія'
  },
  {
    question: 'Як виглядають розриви у фракталі Ньютона?',
    options: ['У вигляді гострих, нерівних країв', 'У вигляді плавних переходів', 'У вигляді концентричних кіл', 'У вигляді прямих ліній'],
    answer: 'У вигляді гострих, нерівних країв'
  },
  {
    question: 'Яка математична властивість визначає складність фракталу Ньютона?',
    options: ['Вимірність', 'Неперервність', 'Диференційованість', 'Інтегрованість'],
    answer: 'Вимірність'
  },
  {
    question: 'Яку область комплексної площини найчастіше досліджують при генерації фрактала Ньютона?',
    options: ['Тільки дійсні числа', 'Тільки уявні числа', 'Як дійсні, так і уявні числа', 'Область простих чисел'],
    answer: 'Як дійсні, так і уявні числа'
  },
  {
    question: 'Як зображуються критичні точки у фракталі Ньютона?',
    options: ['Яскравим кольором', 'Вони відсутні у фракталі', 'Утворюють спіралі', 'Виглядають як темні області'],
    answer: 'Виглядають як темні області'
  },
  {
    question: 'Яка основна мета методу Ньютона при створенні пов\'язаного з ним фрактала?',
    options: ['Знаходження глобальних максимумів', 'Знаходження фіксованих точок', 'Наближення коренів', 'Аналіз диференціальних'],
    answer: 'Наближення коренів'
  },
  {
    question: 'Яке з наступних тверджень є правильним для фрактала Ньютона?',
    options: ['Це періодична функція', 'Він завжди симетричний', 'Має нескінченну роздільну здатність', 'У нього відсутня самоподібність'],
    answer: 'Має нескінченну роздільну здатність'
  },
  {
    question: 'Які фігури можуть з\'являтися в центральних областях фрактала Ньютона?',
    options: ['Сфери', 'Спіралі', 'Гіперболи', 'Еліпси'],
    answer: 'Спіралі'
  },
];

// Function to generate random questions
function generateQuestions() {
  const quizQuestions = document.getElementById('quizQuestions');
  quizQuestions.innerHTML = '';

  const numQuestions = 10; // Specify the number of questions to display

  const shuffledQuestions = questions.sort(() => Math.random() - 0.5).slice(0, numQuestions);

  shuffledQuestions.forEach((ques, index) => {
    const questionDiv = document.createElement('div');
    questionDiv.classList.add('mb-3');

    const questionTitle = document.createElement('h5');
    questionTitle.textContent = `Q${index + 1}. ${ques.question}`;
    questionDiv.appendChild(questionTitle);

    ques.options.forEach((option, optIndex) => {
      const optionLabel = document.createElement('label');
      optionLabel.classList.add('form-check', 'form-check-inline');

      const radioButton = document.createElement('input');
      radioButton.setAttribute('type', 'radio');
      radioButton.setAttribute('name', `question${index}`);
      radioButton.setAttribute('value', option);
      radioButton.classList.add('form-check-input');

      const optionText = document.createElement('span');
      optionText.textContent = option;
      optionText.style.color = "#2c3e50";
      optionLabel.appendChild(radioButton);
      optionLabel.appendChild(optionText);

      questionDiv.appendChild(optionLabel);
    });

    quizQuestions.appendChild(questionDiv);
  });
}

// Function to check answers
function checkAnswers() {
  const answers = document.querySelectorAll('input[type="radio"]:checked');
  let correct = 0;

    const answerResults = document.querySelectorAll('.answer-result');
    answerResults.forEach((result) => {
        result.remove();
    });

  answers.forEach((ans) => {
    const questionIndex = parseInt(ans.getAttribute('name').replace('question', ''));
    const selectedAnswer = ans.value;
    const questionDiv = ans.parentElement.parentElement;

    const resultDiv = document.createElement('div');
    resultDiv.classList.add('answer-result');

    const resultText = document.createElement('span');
    if (questions[questionIndex].answer === selectedAnswer) {
      resultText.textContent = 'Правильно!';
      resultText.classList.add('text-success');
      correct++;
    } else {
      resultText.textContent = `Неправильно! Правильна відповідь: ${questions[questionIndex].answer}`;
      resultText.classList.add('text-danger');
    }

    resultDiv.appendChild(resultText);
    questionDiv.appendChild(resultDiv);
  });
}

// Trigger generation of questions when modal is shown
const quizModal = document.getElementById('quizModal');
quizModal.addEventListener('show.bs.modal', generateQuestions);


document.addEventListener("DOMContentLoaded", function() {
    const urlParams = new URLSearchParams(window.location.search);

    if (urlParams.get('infomodal') === 'show') {
        var tmp = new bootstrap.Modal(document.getElementById('informationModal'))
        tmp.show();
    }

    else if (urlParams.get('quizmodal') === 'show') {
        var tmp = new bootstrap.Modal(document.getElementById('quizModal'))
        tmp.show();
    }
});
