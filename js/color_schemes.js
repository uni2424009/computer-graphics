var selecting_color = false;
var selecting_box = false;
var box_status = 0;
var x1 = 0;
var y1 = 0;
var pixels_array = [];

const THRESHOLD = 30;

var x2 = 400;
var y2 = 400;

const canvas = document.getElementById('canvas');
const ctx = canvas.getContext('2d');

const virtualPixelSize = 400;

const width = 400; const height = 400;

const scaleX = width / virtualPixelSize;
const scaleY = height / virtualPixelSize;

const scale = Math.min(scaleX, scaleY);

const canvasWidth = virtualPixelSize * scale;
const canvasHeight = virtualPixelSize * scale;

canvas.width = canvasWidth;
canvas.height = canvasHeight;

ctx.setTransform(scale, 0, 0, scale, 0, 0);

ctx.fillStyle = "#ffffff";
ctx.fillRect(0, 0, virtualPixelSize, virtualPixelSize);

document.getElementById("file-explorer-input").style.opacity = '0';
document.getElementById("file-explorer-input").style.width = '0';
document.getElementById("file-explorer-input").style.height = '0';

document.getElementById("file-explorer").addEventListener("click", function(e) {
    e.preventDefault();
    document.getElementById("file-explorer-input").click();
});

document.getElementById("file-explorer-input").addEventListener("change", function() {
    var fileInput = this;

    var file = fileInput.files[0];

    if (file) {
        var reader = new FileReader();

        reader.onload = function(e)
        {
            ctx.fillStyle = "#ffffff";
            ctx.fillRect(0, 0, virtualPixelSize, virtualPixelSize);
            var img = new Image();
            img.onload = function() {
            ctx.drawImage(img, 0, 0, 400, 400);
        };

        img.src = e.target.result;
    };

    reader.readAsDataURL(file);
    }
});

canvas.addEventListener("click", function(e) {
    const rect = canvas.getBoundingClientRect()

    const x = event.clientX - rect.left
    const y = event.clientY - rect.top

    if(selecting_box)
    {
        if(box_status == 1)
        {
            x1 = x;
            y1 = y;
            box_status = 2;
        }
        else if(box_status == 2)
        {
            box_status = 0;
            selecting_box = false;
            x2 = x;
            y2 = y;

            var btn = document.getElementById("areaSelectionButton");
            btn.disabled = false;

            btn.innerText = "Обрати область";

            updatePixels();
        }
    }

    if(selecting_color)
    {
        var pixelData = ctx.getImageData(x, y, 1, 1).data;

        var red = pixelData[0];
        var green = pixelData[1];
        var blue = pixelData[2];

        document.getElementById('red').value = red;
        document.getElementById('green').value = green;
        document.getElementById('blue').value = blue;

        document.getElementById('result').innerText = 'RGB(' + red + ', ' + green + ', ' + blue + ')';

        document.getElementById('colored-box').style.backgroundColor = "rgb(" + red + ", " + green + ", " + blue + ")";

        hsv = rgbToHsv(red, green, blue);

        document.getElementById('hsl-hue').value = hsv.h;
        document.getElementById('hsl-saturation').value = hsv.s;
        document.getElementById('hsl-value').value = hsv.v;

        document.getElementById('hsl-result').innerText = 'HSV(' + hsv.h + ', ' + hsv.s + ', ' + hsv.v + ')';

        selecting_color = false;

        const btn = document.getElementById("colorSelectionButton");
        btn.disabled = false;

        btn.innerText = "Обрати колір";

        updatePixels();
    }
});

function selectColor()
{
    if(selecting_box || selecting_color) return;
    selecting_color = true;

    const btn = document.getElementById("colorSelectionButton");
    btn.disabled = true;

    btn.innerText = "Оберіть колір";
}

function selectArea()
{
    if(selecting_box || selecting_color) return;
    selecting_box = true;
    box_status = 1;

    const btn = document.getElementById("areaSelectionButton");
    btn.disabled = true;

    btn.innerText = "Оберіть область";
}

document.addEventListener('keydown', function(e) {
    if (e.key === 'Escape' || e.keyCode === 27) {
        selecting_color = false;
        selecting_box = false;
        box_status = 0;

        var btn = document.getElementById("colorSelectionButton");
        btn.disabled = false;

        btn.innerText = "Обрати колір";

        btn = document.getElementById("areaSelectionButton");
        btn.disabled = false;

        btn.innerText = "Обрати область";
    }
});

function changeColor() {
    var hue = document.getElementById('hsl-hue').value;
    var saturation = document.getElementById('hsl-saturation').value;
    var value = document.getElementById('hsl-value').value;

    document.getElementById('hsl-result').innerText = 'HSV(' + hue + ', ' + saturation + ', ' + value + ')';

    rgb = hsvToRgb(hue, saturation, value);

    document.getElementById('red').value = rgb.r;
    document.getElementById('green').value = rgb.g;
    document.getElementById('blue').value = rgb.b;

    document.getElementById('result').innerText = 'RGB(' + rgb.r + ', ' + rgb.g + ', ' + rgb.b + ')';
    document.getElementById('colored-box').style.backgroundColor = "rgb(" + rgb.r + ", " + rgb.g + ", " + rgb.b + ")";

    pixels_array.forEach(pixel => {
        setPixelColor(pixel.x, pixel.y, "rgb(" + rgb.r + ", " + rgb.g + ", " + rgb.b + ")");
    });
}

function rgbToHsv(r, g, b) {
    r = Math.min(255, Math.max(0, r)) / 255;
    g = Math.min(255, Math.max(0, g)) / 255;
    b = Math.min(255, Math.max(0, b)) / 255;

    const max = Math.max(r, g, b);
    const min = Math.min(r, g, b);

    const delta = max - min;

    let h, s, v;

    v = max;

    if (max !== 0) {
        s = delta / max;

        if (delta !== 0) {
            if (r === max) {
                h = (g - b) / delta + (g < b ? 6 : 0);
            } else if (g === max) {
                h = (b - r) / delta + 2;
            } else {
                h = (r - g) / delta + 4;
            }

            h /= 6;
            h %= 1;
        } else {
            h = 0;
        }
    } else {
        h = s = 0;
    }

    return { h: Math.round(h * 360), s: Math.round(s * 100), v: Math.round(v * 100) };
}

function hsvToRgb(h, s, v) {
    h = (h % 360 + 360) % 360;
    s = Math.min(100, Math.max(0, s)) / 100;
    v = Math.min(100, Math.max(0, v)) / 100;

    const c = v * s;
    const x = c * (1 - Math.abs((h / 60) % 2 - 1));
    const m = v - c;

    let r, g, b;

    if (h < 60) {
        r = c;
        g = x;
        b = 0;
    } else if (h < 120) {
        r = x;
        g = c;
        b = 0;
    } else if (h < 180) {
        r = 0;
        g = c;
        b = x;
    } else if (h < 240) {
        r = 0;
        g = x;
        b = c;
    } else if (h < 300) {
        r = x;
        g = 0;
        b = c;
    } else {
        r = c;
        g = 0;
        b = x;
    }

    return {
        r: Math.round((r + m) * 255),
        g: Math.round((g + m) * 255),
        b: Math.round((b + m) * 255),
    };
}

function updatePixels()
{
    const minX = Math.min(x1, x2);
    const minY = Math.min(y1, y2);
    const width = Math.abs(x2 - x1);
    const height = Math.abs(y2 - y1);

    var r = document.getElementById('red').value;
    var g = document.getElementById('green').value;
    var b = document.getElementById('blue').value;

    pixels_array = [];

    for(var _x = minX; _x < minX+width; _x++)
    {
        for(var _y = minY; _y < minY+height; _y++)
        {
            var pixelData = ctx.getImageData(_x, _y, 1, 1).data;

            if(Math.abs(r - pixelData[0]) > THRESHOLD) continue;
            if(Math.abs(g - pixelData[1]) > THRESHOLD) continue;
            if(Math.abs(b - pixelData[2]) > THRESHOLD) continue;

            pixels_array.push(Object({x: _x, y: _y}));
        }
    }
}
function setPixelColor(x, y, color) {
    ctx.fillStyle = color;
    ctx.fillRect(x, y, 1, 1);
}

function downloadCanvas() {
    var dataUrl = canvas.toDataURL();

    var downloadLink = document.createElement('a');

    downloadLink.download = 'image.png';

    downloadLink.href = dataUrl;

    document.body.appendChild(downloadLink);

    downloadLink.click();

    document.body.removeChild(downloadLink);
}


// Questions and Answers
const questions = [
  {
    question: 'Які атрибути представляє колірна модель HSV?',
    options: ['Hue, Saturation, Intensity', 'Red, Green, Blue', 'Luminance, Chrominance, Value', 'Cyan, Magenta, Yellow'],
    answer: 'Hue, Saturation, Intensity'
  },
  {
    question: 'Який колір в адитивній моделі RGB утворюється при поєднанні всіх трьох базових кольорів?',
    options: ['Чорний', 'Білий', 'Сірий', 'Блакитний'],
    answer: 'Білий'
  },
  {
    question: 'Що означає компонента "V" у колірній моделі HSV?',
    options: ['Чистота кольору', 'Колірний тон', 'Інтенсивність кольору', 'Значення кольору'],
    answer: 'Інтенсивність кольору'
  },
  {
    question: 'Яка колірна модель в основному використовується для апаратно-орієнтованих цілей, таких як екранні дисплеї?',
    options: ['Lab', 'CMYK', 'XYZ', 'RGB'],
    answer: 'RGB'
  },
  {
    question: 'Яку характеристику кольору представляє канал "L*" у колірній моделі Lab?',
    options: ['Відтінок', 'Яскравість', 'Насиченість', 'Інтенсивність'],
    answer: 'Яскравість'
  },
  {
    question: 'Що означає відсутність усіх кольорів у моделі RGB?',
    options: ['Чорний', 'Білий', 'Червоний', 'Жовтий'],
    answer: 'Чорний'
  },
  {
    question: 'З яких компонентів складається колірна модель Lab?',
    options: ['Red, Green, Blue', 'Luminance, Saturation, Value', 'Hue, Chroma, Lightness', 'L*, a*, b*'],
    answer: 'L*, a*, b*'
  },
  {
    question: 'Яка колірна модель призначена для корекції нелінійності сприйняття кольору людиною?',
    options: ['RGB', 'HSV', 'Lab', 'XYZ'],
    answer: 'Lab'
  },
  {
    question: 'Яка основна функція колірної моделі CMYK?',
    options: ['Відображення на екрані', 'Друк', 'Інтерполяція кольорів', 'Сприйняття кольору'],
    answer: 'Друк'
  },
  {
    question: 'Які кольори відрізняються на 180 градусів у колірній моделі HSV',
    options: ['Червоний - Синій', 'Жовтий - Зелений', 'Синій - Жовтий', 'Червоний - Блакитний'],
    answer: 'Синій - Жовтий'
  },
];

// Function to generate random questions
function generateQuestions() {
  const quizQuestions = document.getElementById('quizQuestions');
  quizQuestions.innerHTML = '';

  const numQuestions = 5; // Specify the number of questions to display

  const shuffledQuestions = questions.sort(() => Math.random() - 0.5).slice(0, numQuestions);

  shuffledQuestions.forEach((ques, index) => {
    const questionDiv = document.createElement('div');
    questionDiv.classList.add('mb-3');

    const questionTitle = document.createElement('h5');
    questionTitle.textContent = `Q${index + 1}. ${ques.question}`;
    questionDiv.appendChild(questionTitle);

    ques.options.forEach((option, optIndex) => {
      const optionLabel = document.createElement('label');
      optionLabel.classList.add('form-check', 'form-check-inline');

      const radioButton = document.createElement('input');
      radioButton.setAttribute('type', 'radio');
      radioButton.setAttribute('name', `question${index}`);
      radioButton.setAttribute('value', option);
      radioButton.classList.add('form-check-input');

      const optionText = document.createElement('span');
      optionText.textContent = option;
      optionText.style.color = "#2c3e50";
      optionLabel.appendChild(radioButton);
      optionLabel.appendChild(optionText);

      questionDiv.appendChild(optionLabel);
    });

    quizQuestions.appendChild(questionDiv);
  });
}

// Function to check answers
function checkAnswers() {
  const answers = document.querySelectorAll('input[type="radio"]:checked');
  let correct = 0;

    const answerResults = document.querySelectorAll('.answer-result');
    answerResults.forEach((result) => {
        result.remove();
    });

  answers.forEach((ans) => {
    const questionIndex = parseInt(ans.getAttribute('name').replace('question', ''));
    const selectedAnswer = ans.value;
    const questionDiv = ans.parentElement.parentElement;

    const resultDiv = document.createElement('div');
    resultDiv.classList.add('answer-result');

    const resultText = document.createElement('span');
    if (questions[questionIndex].answer === selectedAnswer) {
      resultText.textContent = 'Правильно!';
      resultText.classList.add('text-success');
      correct++;
    } else {
      resultText.textContent = `Неправильно! Правильна відповідь: ${questions[questionIndex].answer}`;
      resultText.classList.add('text-danger');
    }

    resultDiv.appendChild(resultText);
    questionDiv.appendChild(resultDiv);
  });
}

// Trigger generation of questions when modal is shown
const quizModal = document.getElementById('quizModal');
quizModal.addEventListener('show.bs.modal', generateQuestions);


document.addEventListener("DOMContentLoaded", function() {
    const urlParams = new URLSearchParams(window.location.search);

    if (urlParams.get('infomodal') === 'show') {
        var tmp = new bootstrap.Modal(document.getElementById('informationModal'))
        tmp.show();
    }

    else if (urlParams.get('quizmodal') === 'show') {
        var tmp = new bootstrap.Modal(document.getElementById('quizModal'))
        tmp.show();
    }
});
