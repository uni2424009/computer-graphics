var iterations = 5
const new_branch_length = 0.33;

const canvas = document.getElementById('canvas');
const ctx = canvas.getContext('2d');

const virtualPixelSize = 400;

const width = 400; const height = 400;

const scaleX = width / virtualPixelSize;
const scaleY = height / virtualPixelSize;

const scale = Math.min(scaleX, scaleY);

const canvasWidth = virtualPixelSize * scale;
const canvasHeight = virtualPixelSize * scale;

canvas.width = canvasWidth;
canvas.height = canvasHeight;

ctx.setTransform(scale, 0, 0, scale, 0, 0);

ctx.fillStyle = "#b4b40a";
ctx.fillRect(0, 0, virtualPixelSize, virtualPixelSize);


function drawLine(x1, y1, x2, y2, stroke = 'black', width = 1) {
    ctx.beginPath();
    ctx.moveTo(x1, y1);
    ctx.lineTo(x2, y2);
    ctx.strokeStyle = stroke;
    ctx.lineWidth = width;
    ctx.stroke();
}

function draw_fractal(point1_x, point1_y, point2_x, point2_y, iteration) {
    drawLine(point1_x, point1_y, point2_x, point2_y);

    if(iteration > iterations) {
        return;
    }

    const x_len = Math.abs(point1_x - point2_x);
    const y_len = Math.abs(point1_y - point2_y);

    const mid_point_x = Math.min(point1_x, point2_x) + x_len/2;
    const mid_point_y = Math.min(point1_y, point2_y) + y_len/2;

    draw_fractal(point1_x, point1_y, mid_point_x, mid_point_y, iteration+1);
    draw_fractal(point2_x, point2_y, mid_point_x, mid_point_y, iteration+1);

    // horizontal line
    if(x_len > y_len)
    {
        draw_fractal(
            mid_point_x, mid_point_y, mid_point_x, mid_point_y+x_len*new_branch_length, iteration+1
        );

        draw_fractal(
            mid_point_x, mid_point_y, mid_point_x, mid_point_y-x_len*new_branch_length, iteration+1
        );
    }
    // vertical line
    else
    {
        draw_fractal(
            mid_point_x, mid_point_y, mid_point_x+y_len*new_branch_length, mid_point_y, iteration+1
        );

        draw_fractal(
            mid_point_x, mid_point_y, mid_point_x-y_len*new_branch_length, mid_point_y, iteration+1
        );
    }
}

draw_fractal(0, 0, 0, virtualPixelSize, 0);
draw_fractal(0, 0, virtualPixelSize, 0, 0);
draw_fractal(virtualPixelSize, virtualPixelSize, virtualPixelSize, 0, 0);
draw_fractal(virtualPixelSize, virtualPixelSize, 0, virtualPixelSize, 0);

// Get the range input and label elements
const rangeInput = document.getElementById('n-range');
const label = document.getElementById('n-label');

label.textContent = `n: ${iterations}`;
rangeInput.value = iterations;

// Add an event listener to the range input
rangeInput.addEventListener('input', updateLabelText);

// Function to update the label text based on the range value
function updateLabelText() {
    label.textContent = `n: ${rangeInput.value}`;
    iterations = rangeInput.value;

    ctx.fillRect(0, 0, virtualPixelSize, virtualPixelSize);
    draw_fractal(0, 0, 0, virtualPixelSize, 0);
    draw_fractal(0, 0, virtualPixelSize, 0, 0);
    draw_fractal(virtualPixelSize, virtualPixelSize, virtualPixelSize, 0, 0);
    draw_fractal(virtualPixelSize, virtualPixelSize, 0, virtualPixelSize, 0);
}












// Questions and Answers
const questions = [
  {
    question: 'Що визначає утворення крижаних фракталів?',
    options: ['Ітеративне застосування генератора', 'Розсіювання тепла в молекулах води', 'Кутовий момент замерзання води', 'Магнітні властивості при температурах '],
    answer: 'Ітеративне застосування генератора'
  },
  {
    question: 'Який діапазон фрактальної розмірності зазвичай спостерігається у крижаних фракталах?',
    options: ['від 2 до 3', 'від 0 до 1', 'від 1 до 2', 'від 3 до 4'],
    answer: 'від 1 до 2'
  },
  {
    question: 'Що включає в себе ітераційний процес у крижаних фракталах?',
    options: ['Випадкове генерування фігур', 'Послідовне заморожування молекул води', 'Багаторазове застосування певного генератора', 'Природне виникнення без визначених закономірностей'],
    answer: 'Багаторазове застосування певного генератора'
  },
  {
    question: 'В якій сфері знаходять практичне застосування крижаних фракталів?',
    options: ['Сільськогосподарські науки', 'Телекомунікації', 'Комп\'ютерна графіка', 'Машинобудування'],
    answer: 'Комп\'ютерна графіка'
  },
  {
    question: 'Які значення може набувати фрактальна розмірність крижаного фракталу?',
    options: ['Тільки цілочисельні значення', 'Завжди менше 1', 'Завжди більша за 2', 'Раціанальні значення залежно від їхньої складності'],
    answer: 'Раціанальні значення залежно від їхньої складності'
  },
  {
    question: 'Який термін визначає нецілочисельні властивості крижаного фракталу?',
    options: ['Лінійна розмірність', 'Цілочисельна розмірність', 'Фрактальна розмірність', 'Геометрична розмірність'],
    answer: 'Фрактальна розмірність'
  },
  {
    question: 'Чого досягає повторення генератора у крижаному фракталі?',
    options: ['Спричиняє танення кристалів льоду', 'Створює симетрію в структурі', 'Зменшує фрактальну складність', 'Підвищує чистоту води'],
    answer: 'Створює симетрію в структурі'
  },
  {
    question: 'Який процес призводить до розвитку складних візерунків у крижаних фракталах?',
    options: ['Хаотичне розташування молекул води', 'Нерівномірне нагрівання під час заморожування', 'Випадкові коливання температури', 'Ітеративне застосування генератора'],
    answer: 'Ітеративне застосування генератора'
  },
  {
    question: 'Що відрізняє крижані фрактали від простих візерунків замерзання?',
    options: ['Їм бракує симетрії', 'Вони демонструють самоподібність', 'Формуються лише в контрольованих умовах', 'Вони однорідні за формою та розміром'],
    answer: 'Вони демонструють самоподібність'
  },
];

// Function to generate random questions
function generateQuestions() {
  const quizQuestions = document.getElementById('quizQuestions');
  quizQuestions.innerHTML = '';

  const numQuestions = 5; // Specify the number of questions to display

  const shuffledQuestions = questions.sort(() => Math.random() - 0.5).slice(0, numQuestions);

  shuffledQuestions.forEach((ques, index) => {
    const questionDiv = document.createElement('div');
    questionDiv.classList.add('mb-3');

    const questionTitle = document.createElement('h5');
    questionTitle.textContent = `Q${index + 1}. ${ques.question}`;
    questionDiv.appendChild(questionTitle);

    ques.options.forEach((option, optIndex) => {
      const optionLabel = document.createElement('label');
      optionLabel.classList.add('form-check', 'form-check-inline');

      const radioButton = document.createElement('input');
      radioButton.setAttribute('type', 'radio');
      radioButton.setAttribute('name', `question${index}`);
      radioButton.setAttribute('value', option);
      radioButton.classList.add('form-check-input');

      const optionText = document.createElement('span');
      optionText.textContent = option;
      optionText.style.color = "#2c3e50";
      optionLabel.appendChild(radioButton);
      optionLabel.appendChild(optionText);

      questionDiv.appendChild(optionLabel);
    });

    quizQuestions.appendChild(questionDiv);
  });
}

// Function to check answers
function checkAnswers() {
  const answers = document.querySelectorAll('input[type="radio"]:checked');
  let correct = 0;

    const answerResults = document.querySelectorAll('.answer-result');
    answerResults.forEach((result) => {
        result.remove();
    });

  answers.forEach((ans) => {
    const questionIndex = parseInt(ans.getAttribute('name').replace('question', ''));
    const selectedAnswer = ans.value;
    const questionDiv = ans.parentElement.parentElement;

    const resultDiv = document.createElement('div');
    resultDiv.classList.add('answer-result');

    const resultText = document.createElement('span');
    if (questions[questionIndex].answer === selectedAnswer) {
      resultText.textContent = 'Правильно!';
      resultText.classList.add('text-success');
      correct++;
    } else {
      resultText.textContent = `Неправильно! Правильна відповідь: ${questions[questionIndex].answer}`;
      resultText.classList.add('text-danger');
    }

    resultDiv.appendChild(resultText);
    questionDiv.appendChild(resultDiv);
  });
}

// Trigger generation of questions when modal is shown
const quizModal = document.getElementById('quizModal');
quizModal.addEventListener('show.bs.modal', generateQuestions);


document.addEventListener("DOMContentLoaded", function() {
    const urlParams = new URLSearchParams(window.location.search);

    if (urlParams.get('infomodal') === 'show') {
        var tmp = new bootstrap.Modal(document.getElementById('informationModal'))
        tmp.show();
    }

    else if (urlParams.get('quizmodal') === 'show') {
        var tmp = new bootstrap.Modal(document.getElementById('quizModal'))
        tmp.show();
    }
});
