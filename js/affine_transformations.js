var grid_size = 20;
var x_axis_distance_grid_lines = 10;
var y_axis_distance_grid_lines = 10;
var x_axis_starting_point = { number: 1, suffix: '' };
var y_axis_starting_point = { number: 1, suffix: '' };

var selected_point = 'O';

var canvas = document.getElementById("canvas");
var ctx = canvas.getContext("2d");
ctx.translate(y_axis_distance_grid_lines*grid_size, x_axis_distance_grid_lines*grid_size);

var canvas_width = canvas.width;
var canvas_height = canvas.height;

var num_lines_x = Math.floor(canvas_height/grid_size);
var num_lines_y = Math.floor(canvas_width/grid_size);

function rotMatrix(theta) {
    return [[Math.cos(theta), -Math.sin(theta)],
            [Math.sin(theta),  Math.cos(theta)]];
}

function scaleMatrix(scale) {
    return [[scale, 0], [0, scale]];
}

function degrees_to_radians(degrees)
{
  var pi = Math.PI;
  return degrees * (pi/180);
}

class Hexagon {
    constructor(_Ox, _Oy, _Ax, _Ay) {
        this.Ox = _Ox;
        this.Oy = _Oy;

        this.rotx = _Ox;
        this.roty = _Oy;

        this.Ax = _Ax;
        this.Ay = _Ay;

        this.calc_points();
    }

    calc_points() {
        var A = multiplyMatrices(rotMatrix(degrees_to_radians(0)),   [[this.Ax - this.Ox], [this.Ay - this.Oy]]);
        A[0][0] += this.Ox; A[1][0] += this.Oy;
        var B = multiplyMatrices(rotMatrix(degrees_to_radians(60)),  [[this.Ax - this.Ox], [this.Ay - this.Oy]]);
        B[0][0] += this.Ox; B[1][0] += this.Oy;
        var C = multiplyMatrices(rotMatrix(degrees_to_radians(120)), [[this.Ax - this.Ox], [this.Ay - this.Oy]]);
        C[0][0] += this.Ox; C[1][0] += this.Oy;
        var D = multiplyMatrices(rotMatrix(degrees_to_radians(180)), [[this.Ax - this.Ox], [this.Ay - this.Oy]]);
        D[0][0] += this.Ox; D[1][0] += this.Oy;
        var E = multiplyMatrices(rotMatrix(degrees_to_radians(240)), [[this.Ax - this.Ox], [this.Ay - this.Oy]]);
        E[0][0] += this.Ox; E[1][0] += this.Oy;
        var F = multiplyMatrices(rotMatrix(degrees_to_radians(300)), [[this.Ax - this.Ox], [this.Ay - this.Oy]]);
        F[0][0] += this.Ox; F[1][0] += this.Oy;

        this.points = {"A": A, "B": B, "C": C, "D": D, "E": E, "F": F}
    }

    recalc_points(angle, scale, rotPointx=this.Ox, rotPointy=this.Oy) {
        if(selected_point != "O")
        {
            rotPointx = this.points[selected_point][0][0];
            rotPointy = this.points[selected_point][1][0];
        }

        var A = this.points["A"];
        A[0][0] -= rotPointx; A[1][0] -= rotPointy;
        A = multiplyMatrices(rotMatrix(degrees_to_radians(angle)),   [[A[0][0]], [A[1][0]]]);
        A = multiplyMatrices(scaleMatrix(scale),                     [[A[0][0]], [A[1][0]]]);
        A[0][0] += rotPointx; A[1][0] += rotPointy;

        var B = this.points["B"];
        B[0][0] -= rotPointx; B[1][0] -= rotPointy;
        B = multiplyMatrices(rotMatrix(degrees_to_radians(angle)),   [[B[0][0]], [B[1][0]]]);
        B = multiplyMatrices(scaleMatrix(scale),                     [[B[0][0]], [B[1][0]]]);
        B[0][0] += rotPointx; B[1][0] += rotPointy;

        var C = this.points["C"];
        C[0][0] -= rotPointx; C[1][0] -= rotPointy;
        C = multiplyMatrices(rotMatrix(degrees_to_radians(angle)),   [[C[0][0]], [C[1][0]]]);
        C = multiplyMatrices(scaleMatrix(scale),                     [[C[0][0]], [C[1][0]]]);
        C[0][0] += rotPointx; C[1][0] += rotPointy;

        var D = this.points["D"];
        D[0][0] -= rotPointx; D[1][0] -= rotPointy;
        D = multiplyMatrices(rotMatrix(degrees_to_radians(angle)),   [[D[0][0]], [D[1][0]]]);
        D = multiplyMatrices(scaleMatrix(scale),                     [[D[0][0]], [D[1][0]]]);
        D[0][0] += rotPointx; D[1][0] += rotPointy;

        var E = this.points["E"];
        E[0][0] -= rotPointx; E[1][0] -= rotPointy;
        E = multiplyMatrices(rotMatrix(degrees_to_radians(angle)),   [[E[0][0]], [E[1][0]]]);
        E = multiplyMatrices(scaleMatrix(scale),                     [[E[0][0]], [E[1][0]]]);
        E[0][0] += rotPointx; E[1][0] += rotPointy;

        var F = this.points["F"];
        F[0][0] -= rotPointx; F[1][0] -= rotPointy;
        F = multiplyMatrices(rotMatrix(degrees_to_radians(angle)),   [[F[0][0]], [F[1][0]]]);
        F = multiplyMatrices(scaleMatrix(scale),                     [[F[0][0]], [F[1][0]]]);
        F[0][0] += rotPointx; F[1][0] += rotPointy;

        this.Ox = (A[0][0] + D[0][0])/2;
        this.Oy = (A[1][0] + D[1][0])/2;

        this.points = {"A": A, "B": B, "C": C, "D": D, "E": E, "F": F}
    }

    draw() {
        draw_coodinates();

        ctx.lineWidth = 1;
        ctx.strokeStyle = "#0000ff";

        var last_point = Object.entries(this.points).at(-1)[1];
        var tmp = point_to_canvas(last_point[0], last_point[1]);
        last_point = tmp;

        ctx.beginPath();
        ctx.moveTo(tmp["x"], tmp["y"]);

        for (const [name, point] of Object.entries(this.points)) {
            tmp = point_to_canvas(point[0][0], point[1][0]);
            ctx.lineTo(tmp["x"], tmp["y"]);
            ctx.moveTo(tmp["x"], tmp["y"]);
        }

        ctx.stroke();
        ctx.font = '16px Arial';
        ctx.textAlign = 'start';

        for (const [name, point] of Object.entries(this.points)) {
            if(name == selected_point)
            {
                ctx.fillStyle = "#00ff00";
            }
            else
            {
                ctx.fillStyle = "#0000ff";
            }

            ctx.beginPath();
            tmp = point_to_canvas(point[0][0], point[1][0]);
            ctx.arc(tmp["x"], tmp["y"], 3, 0, Math.PI*2);
            ctx.fill();

            ctx.fillText(name, tmp["x"] + 8, tmp["y"] + 8);
        }

        if("O" == selected_point)
        {
            ctx.fillStyle = "#00ff00";
        }
        else
        {
            ctx.fillStyle = "#0000ff";
        }
        ctx.beginPath();
        tmp = point_to_canvas(this.Ox, this.Oy);
        ctx.arc(tmp["x"], tmp["y"], 3, 0, Math.PI*2);
        ctx.fill();

        ctx.fillText("O", tmp["x"] + 8, tmp["y"] + 8);
    }
}

var current_hexagon = new Hexagon(0, 0, 0, 2);

function multiplyMatrices(m1, m2) {
    var result = [];
    for (var i = 0; i < m1.length; i++) {
        result[i] = [];
        for (var j = 0; j < m2[0].length; j++) {
            var sum = 0;
            for (var k = 0; k < m1[0].length; k++) {
                sum += m1[i][k] * m2[k][j];
            }
            result[i][j] = sum;
        }
    }
    return result;
}

function point_to_canvas(x, y) {
    return {"x": grid_size*x, "y": -grid_size*y};
}

function draw_coodinates() {
    ctx = canvas.getContext("2d");
    ctx.translate(-y_axis_distance_grid_lines*grid_size, -x_axis_distance_grid_lines*grid_size);
    ctx.fillStyle = "#ffffff";
    ctx.fillRect(0, 0, canvas_width, canvas_height);

    // Draw grid lines along X-axis
    for(var i=0; i<=num_lines_x; i++) {
        ctx.beginPath();
        ctx.lineWidth = 1;
        
        // If line represents X-axis draw in different color
        if(i == x_axis_distance_grid_lines) 
            ctx.strokeStyle = "#000000";
        else
            ctx.strokeStyle = "#e9e9e9";
        
        if(i == num_lines_x) {
            ctx.moveTo(0, grid_size*i);
            ctx.lineTo(canvas_width, grid_size*i);
        }
        else {
            ctx.moveTo(0, grid_size*i+0.5);
            ctx.lineTo(canvas_width, grid_size*i+0.5);
        }
        ctx.stroke();
    }

    // Draw grid lines along Y-axis
    for(var i=0; i<=num_lines_y; i++) {
        ctx.beginPath();
        ctx.lineWidth = 1;
        
        // If line represents X-axis draw in different color
        if(i == y_axis_distance_grid_lines) 
            ctx.strokeStyle = "#000000";
        else
            ctx.strokeStyle = "#e9e9e9";
        
        if(i == num_lines_y) {
            ctx.moveTo(grid_size*i, 0);
            ctx.lineTo(grid_size*i, canvas_height);
        }
        else {
            ctx.moveTo(grid_size*i+0.5, 0);
            ctx.lineTo(grid_size*i+0.5, canvas_height);
        }
        ctx.stroke();
    }

    ctx.translate(y_axis_distance_grid_lines*grid_size, x_axis_distance_grid_lines*grid_size);

    // Ticks marks along the positive X-axis
    for(var i=1; i<=(num_lines_y - y_axis_distance_grid_lines); i++) {
        if(i == (num_lines_y - y_axis_distance_grid_lines))
        {
            ctx.beginPath();
            ctx.lineWidth = 1;
            ctx.strokeStyle = "#000000";

            ctx.moveTo(grid_size*i, 1);
            ctx.lineTo(grid_size*i-10, -4);

            ctx.moveTo(grid_size*i, 1);
            ctx.lineTo(grid_size*i-10, 5);
            ctx.stroke();


            ctx.font = '9px Arial';
            ctx.textAlign = 'start';
            ctx.fillStyle = "#000000";
            ctx.fillText("X", grid_size*i-10, 15);
        }
        else
        {
            ctx.beginPath();
            ctx.lineWidth = 1;
            ctx.strokeStyle = "#000000";

            // Draw a tick mark 6px long (-3 to 3)
            ctx.moveTo(grid_size*i+0.5, -3);
            ctx.lineTo(grid_size*i+0.5, 3);
            ctx.stroke();

            // Text value at that point
            ctx.font = '9px Arial';
            ctx.textAlign = 'start';
            ctx.fillStyle = "#000000";
            ctx.fillText(x_axis_starting_point.number*i + x_axis_starting_point.suffix, grid_size*i-2, 15);
        }
    }

    // Ticks marks along the negative X-axis
    for(var i=1; i<y_axis_distance_grid_lines; i++) {
        ctx.beginPath();
        ctx.lineWidth = 1;
        ctx.strokeStyle = "#000000";

        // Draw a tick mark 6px long (-3 to 3)
        ctx.moveTo(-grid_size*i+0.5, -3);
        ctx.lineTo(-grid_size*i+0.5, 3);
        ctx.stroke();

        // Text value at that point
        ctx.font = '9px Arial';
        ctx.textAlign = 'end';
        ctx.fillStyle = "#000000";
        ctx.fillText(-x_axis_starting_point.number*i + x_axis_starting_point.suffix, -grid_size*i+3, 15);
    }

    // Ticks marks along the positive Y-axis
    // Positive Y-axis of graph is negative Y-axis of the canvas
    for(var i=1; i<(num_lines_x - x_axis_distance_grid_lines); i++) {
        ctx.beginPath();
        ctx.lineWidth = 1;
        ctx.strokeStyle = "#000000";

        // Draw a tick mark 6px long (-3 to 3)
        ctx.moveTo(-3, grid_size*i+0.5);
        ctx.lineTo(3, grid_size*i+0.5);
        ctx.stroke();

        // Text value at that point
        ctx.font = '9px Arial';
        ctx.textAlign = 'start';
        ctx.fillStyle = "#000000";
        ctx.fillText(-y_axis_starting_point.number*i + y_axis_starting_point.suffix, 8, grid_size*i+3);
    }

    // Ticks marks along the negative Y-axis
    // Negative Y-axis of graph is positive Y-axis of the canvas
    for(var i=1; i<=x_axis_distance_grid_lines; i++) {
        if(i == x_axis_distance_grid_lines)
        {
            ctx.beginPath();
            ctx.lineWidth = 1;
            ctx.strokeStyle = "#000000";

            ctx.moveTo(1, -grid_size*i+1);
            ctx.lineTo(5, -grid_size*i+11);

            ctx.moveTo(1, -grid_size*i+1);
            ctx.lineTo(-4, -grid_size*i+11);
            ctx.stroke();

            // Text value at that point
            ctx.font = '9px Arial';
            ctx.textAlign = 'start';
            ctx.fillStyle = "#000000";

            ctx.fillText("Y", 8, -grid_size*i+10);
        }
        else
        {
            ctx.beginPath();
            ctx.lineWidth = 1;
            ctx.strokeStyle = "#000000";

            // Draw a tick mark 6px long (-3 to 3)
            ctx.moveTo(-3, -grid_size*i+0.5);
            ctx.lineTo(3, -grid_size*i+0.5);
            ctx.stroke();

            // Text value at that point
            ctx.font = '9px Arial';
            ctx.textAlign = 'start';
            ctx.fillStyle = "#000000";

            ctx.fillText(y_axis_starting_point.number*i + y_axis_starting_point.suffix, 8, -grid_size*i+3);
        }
    }
}

draw_coodinates();
current_hexagon.draw();

const inputFields = document.querySelectorAll('input[type="number"]');

function checkAllFieldsFilled() {
    for (const field of inputFields) {
        if (field.value.trim() === '') {
            return false;
        }
    }

    return true;
}

var n = 0;

const scale_x = document.getElementById('scale-x');
scale_x.textContent = "1.00";

const scale_y = document.getElementById('scale-y');
scale_y.textContent = "1.00";

const angle1 = document.getElementById('angle-1');
angle1.textContent = "0.0";

const angle2 = document.getElementById('angle-2');
angle2.textContent = "0.0";

const angle3 = document.getElementById('angle-3');
angle3.textContent = "0.0";

const angle4 = document.getElementById('angle-4');
angle4.textContent = "0.0";

const letter1 = document.getElementById('letter-1');
letter1.textContent = "O";

const letter2 = document.getElementById('letter-2');
letter2.textContent = "O";

const letter3 = document.getElementById('letter-3');
letter3.textContent = "O";

const letter4 = document.getElementById('letter-4');
letter4.textContent = "O";

const rotationSelect = document.getElementById('point-of-rotation');
rotationSelect.value = "O";

rotationSelect.addEventListener('change', updateSelection);

function updateSelection() {
    selected_point = rotationSelect.value;
    current_hexagon.recalc_points(0, 1);
    current_hexagon.draw();

    letter1.textContent = selected_point;
    letter2.textContent = selected_point;
    letter3.textContent = selected_point;
    letter4.textContent = selected_point;
}


const rangeInput = document.getElementById('n-range');
const label = document.getElementById('n-label');

label.textContent = `Кут: ${n}°`;
rangeInput.value = n;

function handleInputChange() {
    if (checkAllFieldsFilled()) {
        var o_x = parseInt(document.getElementById('o_x').value);
        var o_y = parseInt(document.getElementById('o_y').value);

        var a_x = parseInt(document.getElementById('a_x').value);
        var a_y = parseInt(document.getElementById('a_y').value);

        current_hexagon = new Hexagon(o_x, o_y, a_x, a_y);
        current_hexagon.draw();

        n = 0;
        label.textContent = `Кут: ${n}°`;
        rangeInput.value = n;
    }
}

inputFields.forEach(field => {
    field.addEventListener('input', handleInputChange);
});

// Add an event listener to the range input
rangeInput.addEventListener('input', updateLabelText);

// Function to update the label text based on the range value
function updateLabelText() {
    label.textContent = `Кут: ${rangeInput.value}°`;
    var diff = rangeInput.value - n;
    diff /= 360;

    scale_x.textContent = (1 + rangeInput.value/360).toFixed(2);
    scale_y.textContent = (1 + rangeInput.value/360).toFixed(2);

    angle1.textContent = parseFloat(rangeInput.value).toFixed(1);
    angle2.textContent = parseFloat(rangeInput.value).toFixed(1);
    angle3.textContent = parseFloat(rangeInput.value).toFixed(1);
    angle4.textContent = parseFloat(rangeInput.value).toFixed(1);

    var scale = 1;

    if(diff < 0)
    {
        scale = 1/(1 - diff);
    }
    else
    {
        scale = 1 + diff;
    }

    current_hexagon.recalc_points(diff*360, scale);
    current_hexagon.draw();

    n = rangeInput.value;
}


function downloadCanvas() {
    var dataUrl = canvas.toDataURL();

    var downloadLink = document.createElement('a');

    downloadLink.download = 'image.png';

    downloadLink.href = dataUrl;

    document.body.appendChild(downloadLink);

    downloadLink.click();

    document.body.removeChild(downloadLink);
}




// Questions and Answers
const questions = [
  {
    question: 'Яка властивість зберігається при афінних перетвореннях?',
    options: ['Кути', 'Паралельні прямі', 'Відношення відстаней між точками', 'Кривизна'],
    answer: 'Паралельні прямі'
  },
  {
    question: 'Яке з наступних перетворень змінює розмір та орієнтацію об\'єкта?',
    options: ['Переміщення', 'Обертання', 'Відображення', 'Масштабування'],
    answer: 'Масштабування'
  },
  {
    question: 'Що визначає визначник матриці, яка представляє афінне перетворення, про це перетворення?',
    options: ['Наявність обертання', 'Наявність масштабування', 'Наявність трансляції', 'Наявність відображення'],
    answer: 'Наявність масштабування'
  },
  {
    question: 'Яке перетворення представляється матрицею, що має ненульові значення тільки в елементах, розташованих поза діагоналлю?',
    options: ['Поворот', 'Масштабування', 'Зсув', 'Транспонування'],
    answer: 'Зсув'
  },
  {
    question: 'До якого типу перетворень відноситься діагональна матриця з різними значеннями по діагоналі?',
    options: ['Перенос', 'Поворот', 'Масштабування', 'Відображення'],
    answer: 'Масштабування'
  },
  {
    question: 'Що відбувається з визначником матриці, яка представляє афінне перетворення, що виконує відображення?',
    options: ['Стає від\'ємним', 'Стає рівним нулю', 'Залишається незмінним', 'Подвоюється'],
    answer: 'Стає від\'ємним'
  },
  {
    question: 'Яке перетворення змінює положення об\'єкта без зміни його форми або розміру?',
    options: ['Масштабування', 'Поворот', 'Переміщення', 'Зсув'],
    answer: 'Переміщення'
  },
  {
    question: 'Що таке афінне перетворення в математиці?',
    options: ['Перетворення, яке зберігає паралельні прямі', 'Перетворення, яке змінює розмір об\'єкта', 'Перетворення, яке лише повертає фігури', 'Перетворення, яке спотворює фігури випадковим чином'],
    answer: 'Перетворення, яке зберігає паралельні прямі'
  },
  {
    question: 'Що з переліченого НЕ є різновидом афінного перетворення?',
    options: ['Проекція', 'Зсув', 'Поворот', 'Збільшення'],
    answer: 'Проекція'
  },
  {
    question: '    Чому дорівнює визначник матриці, що представляє чисте обертання у двовимірному афінному перетворенні?',
    options: ['1', '0', '-1', 'Залежить від кута повороту'],
    answer: '1'
  },
];

// Function to generate random questions
function generateQuestions() {
  const quizQuestions = document.getElementById('quizQuestions');
  quizQuestions.innerHTML = '';

  const numQuestions = 5; // Specify the number of questions to display

  const shuffledQuestions = questions.sort(() => Math.random() - 0.5).slice(0, numQuestions);

  shuffledQuestions.forEach((ques, index) => {
    const questionDiv = document.createElement('div');
    questionDiv.classList.add('mb-3');

    const questionTitle = document.createElement('h5');
    questionTitle.textContent = `Q${index + 1}. ${ques.question}`;
    questionDiv.appendChild(questionTitle);

    ques.options.forEach((option, optIndex) => {
      const optionLabel = document.createElement('label');
      optionLabel.classList.add('form-check', 'form-check-inline');

      const radioButton = document.createElement('input');
      radioButton.setAttribute('type', 'radio');
      radioButton.setAttribute('name', `question${index}`);
      radioButton.setAttribute('value', option);
      radioButton.classList.add('form-check-input');

      const optionText = document.createElement('span');
      optionText.textContent = option;
      optionText.style.color = "#2c3e50";
      optionLabel.appendChild(radioButton);
      optionLabel.appendChild(optionText);

      questionDiv.appendChild(optionLabel);
    });

    quizQuestions.appendChild(questionDiv);
  });
}

// Function to check answers
function checkAnswers() {
  const answers = document.querySelectorAll('input[type="radio"]:checked');
  let correct = 0;

    const answerResults = document.querySelectorAll('.answer-result');
    answerResults.forEach((result) => {
        result.remove();
    });

  answers.forEach((ans) => {
    const questionIndex = parseInt(ans.getAttribute('name').replace('question', ''));
    const selectedAnswer = ans.value;
    const questionDiv = ans.parentElement.parentElement;

    const resultDiv = document.createElement('div');
    resultDiv.classList.add('answer-result');

    const resultText = document.createElement('span');
    if (questions[questionIndex].answer === selectedAnswer) {
      resultText.textContent = 'Правильно!';
      resultText.classList.add('text-success');
      correct++;
    } else {
      resultText.textContent = `Неправильно! Правильна відповідь: ${questions[questionIndex].answer}`;
      resultText.classList.add('text-danger');
    }

    resultDiv.appendChild(resultText);
    questionDiv.appendChild(resultDiv);
  });
}

// Trigger generation of questions when modal is shown
const quizModal = document.getElementById('quizModal');
quizModal.addEventListener('show.bs.modal', generateQuestions);


document.addEventListener("DOMContentLoaded", function() {
    const urlParams = new URLSearchParams(window.location.search);

    if (urlParams.get('infomodal') === 'show') {
        var tmp = new bootstrap.Modal(document.getElementById('informationModal'))
        tmp.show();
    }

    else if (urlParams.get('quizmodal') === 'show') {
        var tmp = new bootstrap.Modal(document.getElementById('quizModal'))
        tmp.show();
    }
});
